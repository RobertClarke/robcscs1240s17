"use strict"
var a = Math.floor(Math.random()*5);
var b = Math.floor(Math.random()*3);
var c = Math.floor(Math.random()*9);
var d = Math.floor(Math.random()*8);
var e = Math.floor(Math.random()*9);
var f = Math.floor(Math.random()*19);
var Crusts = {0:"Hand Tossed",1:"Handmade Pan",2:"Crunchy Thin Crust",3:"Brooklyn Style",4:"Gluten free Crust"};
var Sizes = {0:"Small",1:"Medium",2:"Large"};
var Sides = {0:"BBQ Sauce",1:"Balsamic",2:"Blue Cheese Sauce",3:"Caesar",4:"Icing",5:"kicker Hot Sauce",6:"Marinara",7:"Ranch",8:"Sweet Mango Habanero Sauce"};
var Sauces =  {0:"Alfredo Sauce",1:"BBQ Sauce",2:"Blue Cheese Sauce",3:"Garlic Parmesan White Sauce",4:"Hot Sauce",5:"Mango Habanero Sauce",6:"Ranch Dressing",7:"Robust Inspired Tomato Sauce"};
var Meats = {0:"Bacon",1:"beef",2:"Ham",3:"Italian Sausage",4:"Pepperoni",5:"Philly Steak",6:"Premium Chicken",7:"Salami",8:"Sliced Italian Sausage"};
var NonMeats = {0:"American Cheese",1:"Banana Peppers",2:"Black Olives",3:"Cheddar Cheese",4:"Cheese",5:"Diced Tomatoes",6:"Feta Cheese",7:"Green Peppers",8:"Hot Sauce",9:"Jalapeno Peppers",10:"Mushrooms",11:"Onions",12:"Pineapple",13:"Roasted Red Peppers",14:"Shredded Parmesan",15:"Shredded Provolone Cheese",16:"Sliced Provolone Cheese",17:"Sliced Provolone",18:"Spinach"};
console.log(Sizes[b],Crusts[a]+" with",Sauces[d]+" topped with",Meats[e]+" and",NonMeats[f]+" with a side of",Sides[c]);